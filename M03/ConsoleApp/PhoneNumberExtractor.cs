﻿using System;
using System.IO;

namespace ConsoleApp
{
    public class PhoneNumberExtractor
    {
        internal static void Extract(string inputFilePath, string outputFilePath)
        {
            if (!File.Exists(inputFilePath))
            {
                Console.WriteLine("The input file doesn't exist.");
                return;
            }
            var text = File.ReadAllText(inputFilePath);
            var numbers = PhoneNumberParser.Parse(text);
            using var writer = new StreamWriter(outputFilePath);
            foreach (var number in numbers)
            {
                writer.WriteLine(number);
            }
        }
    }
}