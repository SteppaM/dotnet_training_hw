﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    internal class PhoneNumberParser
    {
        public static List<string> Parse(string text)
        {
            if (text == null) throw new ArgumentNullException();

            var regex1 = new Regex(@"[+]*\d? [(]?\d{3}[)]? \d{3}-\d{2}-\d{2}");
            var regex2 = new Regex(@"[+]\d{3} [(]\d{2}[)] \d{3}-\d{4}");

            var matches1 = regex1.Matches(text);
            var matches2 = regex2.Matches(text);

            return matches1.Concat(matches2).Select(x => x.Value).ToList();
        }
    }
}