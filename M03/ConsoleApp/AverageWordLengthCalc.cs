﻿using System;

namespace ConsoleApp
{
    internal class AverageWordLengthCalc
    {
        public static double Calc(string text)
        {
            if (text == null) throw new ArgumentNullException(nameof(text));

            var wordsCount = 0;
            var totalCharCount = 0;
            var currentCharCount = 0;
            var i = 0;

            while (i < text.Length)
            {
                if (char.IsWhiteSpace(text[i]) || char.IsSeparator(text[i]) || char.IsPunctuation(text[i]))
                {
                    if (currentCharCount > 0)
                    {
                        totalCharCount += currentCharCount;
                        wordsCount += 1;
                        currentCharCount = 0;
                    }
                }
                else
                {
                    currentCharCount += 1;
                }

                i += 1;
            }

            if (currentCharCount > 0)
            {
                totalCharCount += currentCharCount;
                wordsCount += 1;
            }

            if (wordsCount == 0) return 0;
            return (double) totalCharCount / wordsCount;
        }
    }
}
