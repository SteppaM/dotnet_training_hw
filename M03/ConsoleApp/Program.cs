﻿using System;
using System.IO;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine(AverageWordLengthCalc.Calc(""));
            Console.WriteLine(AverageWordLengthCalc.Calc("!!! ,   asdf  asd asd ,asdf  "));
            Console.WriteLine(Doubler.Double("omg i love shrek", "o kek"));
            Console.WriteLine(BigIntCalc.Sum("9999999999", "9999999999"));
            Console.WriteLine(BigIntCalc.Sum("19999999999", "9999999999"));
            Console.WriteLine(BigIntCalc.Sum("100j", "9f"));
            Console.WriteLine(WordReverser.Reverse("The greatest victory is that which requires no battle"));

            var inputPath = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\..\", "Text.txt"));
            var outputPath = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"..\..\..\", "Numbers.txt"));
            PhoneNumberExtractor.Extract(inputPath, outputPath);
        }
    }
}
