﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    internal class WordReverser
    {
        public static string Reverse(string text)
        {
            if (text == null) throw new ArgumentNullException(nameof(text));
            return string.Join(' ', new Regex(@"[\d\w]+", RegexOptions.RightToLeft).Matches(text).Select(x => x.Value));
        }
    }
}