﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp
{
    public class Doubler
    {
        internal static string Double(string stringToDouble, string charsToDouble)
        {
            if (stringToDouble == null) throw new ArgumentNullException();
            if (charsToDouble == null) throw new ArgumentNullException();

            var charsToDoubleSet = new HashSet<char>(charsToDouble.Where(x => !(char.IsWhiteSpace(x) || char.IsSeparator(x))));
            var sb = new StringBuilder();
            foreach (var character in stringToDouble)
            {
                if (charsToDoubleSet.Contains(character))
                {
                    sb.Append(character);
                }
                sb.Append(character);
            }

            return sb.ToString();
        }
    }
}
