﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    internal class BigIntCalc
    {
        public static string Sum(string a, string b)
        {
            ValidateStrNumber(a);
            ValidateStrNumber(b);

            // adding leading zeroes to a smaller string.
            if (a.Length > b.Length)
                b = new string('0', a.Length - b.Length) + b;
            else
                a = new string('0', b.Length - a.Length) + a;

            var result = new List<int>();
            var transferDigit = 0;

            for (var i = a.Length-1; i >= 0; i--)
            {
                var digitFromA = a[i] - '0';
                var digitFromB = b[i] - '0';

                var singleDigitSum = digitFromA + digitFromB + transferDigit;
                result.Add(singleDigitSum % 10);
                transferDigit = singleDigitSum / 10;
            }

            if (transferDigit == 1)
                result.Add(transferDigit);

            result.Reverse();
            return string.Concat(result);
        }

        private static void ValidateStrNumber(string number)
        {
            if (number == null) throw new ArgumentNullException(nameof(number), "Input string is null.");
            if (number.Length == 0) throw new ArgumentException("Input string is empty.");
            var correctFormatRegex = new Regex(@"^[-+]?\d+$");
            if (!correctFormatRegex.IsMatch(number))
                throw new FormatException("Input string was not in a correct format.");
        }
    }
}