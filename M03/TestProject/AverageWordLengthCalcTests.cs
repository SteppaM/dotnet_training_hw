using NUnit.Framework;
using ConsoleApp;

namespace TestProject
{
    [TestFixture]
    public class AverageWordLengthCalcTests
    {
        [TestCase(null)]
        public void Calc_Null_ThrowsException(string str)
        {
            Assert.That(() => AverageWordLengthCalc.Calc(str), Throws.ArgumentNullException);
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase("@!")]
        [TestCase(" ! ")]
        public void Calc_StringWithoutWords_ReturnsZero(string str)
        {
            Assert.That(AverageWordLengthCalc.Calc(str), Is.Zero);
        }

        [TestCase("word, word!", 4)]
        public void Calc_StringWithWords_ReturnsAverageWordLength(string str, int expectedLength)
        {
            Assert.That(AverageWordLengthCalc.Calc(str), Is.EqualTo(expectedLength));
        }
    }
}