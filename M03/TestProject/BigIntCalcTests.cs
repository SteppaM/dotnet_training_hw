﻿using System;
using ConsoleApp;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class BigIntCalcTests
    {
        [TestCase(null, null)]
        [TestCase("123", null)]
        [TestCase(null, "123")]
        public void Sum_NullArgs_ThrowsException(string number1, string number2)
        {
            Assert.That(() => BigIntCalc.Sum(number1, number2), Throws.ArgumentNullException);
        }

        [TestCase("", "")]
        [TestCase("123", "")]
        [TestCase("", "123")]
        public void Sum_EmptyStringArgs_ThrowsException(string number1, string number2)
        {
            Assert.That(() => BigIntCalc.Sum(number1, number2), Throws.ArgumentException);
        }

        [TestCase("as22df", "!@123")]
        [TestCase("123", "a2b")]
        [TestCase("a2b", "123")]
        public void Sum_IncorrectFormatArgs_ThrowsException(string number1, string number2)
        {
            Assert.That(() => BigIntCalc.Sum(number1, number2), Throws.Exception.TypeOf<FormatException>());
        }

        [TestCase("0", "0", "0")]
        [TestCase("11", "11", "22")]
        [TestCase("99", "99", "198")]
        public void Sum_CorrectArgs_ExpectedResult(string number1, string number2, string expectedResult)
        {
            Assert.That(BigIntCalc.Sum(number1, number2), Is.EqualTo(expectedResult));
        }
    }
}
