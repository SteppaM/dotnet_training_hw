﻿using ConsoleApp;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class DoublerTests
    {
        [TestCase(null, null)]
        [TestCase("o", null)]
        [TestCase(null, "o")]
        public void Double_NullArgs_ThrowsException(string stringToDouble, string charsToDouble)
        {
            Assert.That(() => Doubler.Double(stringToDouble, charsToDouble), Throws.ArgumentNullException);
        }

        [TestCase("", "", "")]
        [TestCase("kek", "", "kek")]
        [TestCase(" kek ", " ", " kek ")]
        [TestCase("", "kek", "")]
        [TestCase("omg i love shrek", "", "omg i love shrek")]
        [TestCase("omg i love shrek", "o kek", "oomg i loovee shreekk")]
        public void Double_ValidArgs_ExpectedResult(string stringToDouble, string charsToDouble, string expectedResult)
        {
            Assert.That(Doubler.Double(stringToDouble, charsToDouble), Is.EqualTo(expectedResult));
        }
    }
}
