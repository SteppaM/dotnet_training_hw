﻿using ConsoleApp;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class WordReverserTests
    {
        [TestCase(null)]
        public void Reverse_NullArg_ThrowsException(string text)
        {
            Assert.That(() => WordReverser.Reverse(text), Throws.ArgumentNullException);
        }

        [TestCase("")]
        public void Reverse_EmptyStringArg_EmptyResult(string text)
        {
            Assert.That(WordReverser.Reverse(text), Is.Empty);
        }

        [Test]
        public void Reverse_ValidArg_ExpectedResult()
        {
            var actualResult = WordReverser.Reverse("The greatest victory is that which requires no battle");
            var expectedResult = "battle no requires which that is victory greatest The";
            Assert.That((actualResult), Is.EqualTo(expectedResult));
        }
    }
}
