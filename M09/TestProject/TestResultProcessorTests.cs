﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApp;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    internal class TestResultProcessorTests
    {
        [Test]
        public void ProcessTestResults_InvalidOptionName_ThrowsException()
        {
            var options = new[] { "asd", "Ivan" };
            Assert.That(() => new TestResultProcessor(options), Throws.Exception.TypeOf<ArgumentException>());
        }

        [Test]
        public void ProcessTestResults_InvalidArgumentFormat_ThrowsException()
        {
            var options = new[] { "-datefrom", "asd"};
            Assert.That(() => new TestResultProcessor(options), Throws.Exception.TypeOf<FormatException>());
        }

        [Test]
        public void ProcessTestResults_EmptyArgument_ThrowsException()
        {
            var options = new[] { "-datefrom" };
            Assert.That(() => new TestResultProcessor(options), Throws.Exception.TypeOf<ArgumentException>());
        }

        [Test]
        public void ProcessTestResults_ValidSortOptions_Sorts()
        {
            var options = new[] { "-sort", "mark", "asc" };
            var results = new List<StudentTest>()
            {
                new StudentTest()
                    { Mark = 5 },
                new StudentTest()
                    { Mark = 4 },
                new StudentTest()
                    { Mark = 2 },
            };

            var processor = new TestResultProcessor(options);
            var processedResults = processor.ProcessTestResults(results).ToList();
            Assert.That(processedResults[0].Mark, Is.EqualTo(2));
        }

        [Test]
        public void ProcessTestResults_InvalidArgs_ThrowsException()
        {
            var options = new[] { "-sort", "asd", "asc" };
            Assert.That(() => new TestResultProcessor(options), Throws.Exception.TypeOf<ArgumentException>());
        }


        [Test]
        public void ProcessTestResults_FilterByName()
        {
            var options = new[] {"-name", "Ivan"};
            var results = new List<StudentTest>()
            {
                new StudentTest()
                    { Name = "Ivan" },
                new StudentTest()
                    { Name = "Andrey" },
            };

            var processor = new TestResultProcessor(options);
            var processedResults = processor.ProcessTestResults(results).ToList();
            Assert.That(processedResults.Count, Is.EqualTo(1));
            Assert.That(processedResults[0].Name, Is.EqualTo("Ivan"));
        }

        [Test]
        public void ProcessTestResults_FilterByMinMark()
        {
            var options = new[] { "-minmark", "5" };
            var results = new List<StudentTest>()
            {
                new StudentTest()
                    { Mark = 4 },
                new StudentTest()
                    { Mark = 5 },
            };

            var processor = new TestResultProcessor(options);
            var processedResults = processor.ProcessTestResults(results).ToList();
            Assert.That(processedResults.Count, Is.EqualTo(1));
            Assert.That(processedResults[0].Mark, Is.EqualTo(5));
        }

        [Test]
        public void ProcessTestResults_FilterByMaxMark()
        {
            var options = new[] { "-maxmark", "4" };
            var results = new List<StudentTest>()
            {
                new StudentTest()
                    { Mark = 4 },
                new StudentTest()
                    { Mark = 5 },
            };

            var processor = new TestResultProcessor(options);
            var processedResults = processor.ProcessTestResults(results).ToList();
            Assert.That(processedResults.Count, Is.EqualTo(1));
            Assert.That(processedResults[0].Mark, Is.EqualTo(4));
        }

        [Test]
        public void ProcessTestResults_FilterByDateFrom()
        {
            var options = new[] { "-datefrom", "01/01/2020" };
            var expectedDate = new DateTime(2020, 1, 1);
            var results = new List<StudentTest>()
            {
                new StudentTest()
                    { Date = new DateTime(2020, 1, 1)},
                new StudentTest()
                    { Date = new DateTime(2019, 1, 1)},
            };

            var processor = new TestResultProcessor(options);
            var processedResults = processor.ProcessTestResults(results).ToList();
            Assert.That(processedResults.Count, Is.EqualTo(1));
            Assert.That(processedResults[0].Date, Is.EqualTo(expectedDate));
        }
    }
}
