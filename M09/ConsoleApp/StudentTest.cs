﻿using ConsoleApp.JsonConverters;
using System;
using System.Text.Json.Serialization;

namespace ConsoleApp
{
    internal class StudentTest
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("test")]
        public string Test { get; set; }
        [JsonPropertyName("date")]
        [JsonConverter(typeof(DateTimeConverter))]
        public DateTime Date { get; set; }
        [JsonPropertyName("mark")]
        public int Mark { get; set; }
    }
}