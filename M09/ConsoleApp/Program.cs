﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] options)
        {
            var filePath = @"..\..\..\..\TestResults.json";
            var jsonTestResults = File.ReadAllText(filePath);
            var testResults = JsonSerializer.Deserialize<List<StudentTest>>(jsonTestResults);
            var testResultProcessor = new TestResultProcessor(options);
            var processedResults = testResultProcessor.ProcessTestResults(testResults);
            foreach (var result in processedResults)
                Console.WriteLine($"{result.Name,-10} {result.Test,-15} {result.Date:dd/MM/yyyy} {result.Mark}");;
        }
    }
}
