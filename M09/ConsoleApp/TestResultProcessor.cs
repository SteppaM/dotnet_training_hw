﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    internal class TestResultProcessor
    {
        public string Name { get; private set; }
        public string Test { get; private set; }
        public int MinMark { get; private set; } = int.MinValue;
        public int MaxMark { get; private set; } = int.MaxValue;
        public DateTime DateFrom { get; private set; } = DateTime.MinValue;
        public DateTime DateTo { get; private set; } = DateTime.MaxValue;
        public SortingOptions SortingOptions { get; private set; }

        public static Dictionary<string, (string format, int argsCount)> OptionsMetadata { get; } = 
            new Dictionary<string, (string format, int argsCount)>()
        {
            ["-name"] = ("string literal, example: -name Ivan", 1),
            ["-test"] = ("string literal, example: -test Math", 1),
            ["-minmark"] = ("integer number, example: -minmark 4", 1),
            ["-maxmark"] = ("integer number, example: -maxmark 5", 1),
            ["-dateto"] = ("dd/MM/yyyy", 1),
            ["-datefrom"] = ("dd/MM/yyyy", 1),
            ["-sort"] = ("parameter to sort by, order (asc or desc), example: -sort name asc", 2),
        };

        public TestResultProcessor(IEnumerable<string> options)
        {
            ParseOptions(options);
        }

        public IEnumerable<StudentTest> ProcessTestResults(IEnumerable<StudentTest> results) => SortTestResults(FilterTestResults(results));

        private IEnumerable<StudentTest> FilterTestResults(IEnumerable<StudentTest> results)
        {
            return results
                .Where(testResult =>
                    (Name == null || Name == testResult.Name) 
                    && (Test == null || Test == testResult.Test) 
                    && (testResult.Date >= DateFrom && testResult.Date <= DateTo) 
                    && (testResult.Mark >= MinMark && testResult.Mark <= MaxMark));
        }

        private IEnumerable<StudentTest> SortTestResults(IEnumerable<StudentTest> results)
        {
            if (SortingOptions == null)
                return results;
            
            return SortingOptions.InAscendingOrder
                ? results.OrderBy(x => SortingOptions.PropertyToOrderBy.GetValue(x, null))
                : results.OrderByDescending(x => SortingOptions.PropertyToOrderBy.GetValue(x, null));
        }

        private void ParseOptions(IEnumerable<string> options)
        {
            var stack = new Stack<string>(options.Reverse());
            while (stack.Count > 0)
            {
                var optionName = stack.Pop();
                if (!OptionsMetadata.ContainsKey(optionName))
                {
                    var msg = $"Invalid option name: {optionName}";
                    throw new ArgumentException(msg);
                }
                if (stack.Count < OptionsMetadata[optionName].argsCount)
                {
                    var msg = $"Wrong number of arguments.\n" +
                              $"Required number of arguments for option {optionName}: {OptionsMetadata[optionName].argsCount}";
                    throw new ArgumentException(msg);
                }

                if (optionName == "-name")
                        Name = stack.Pop();
                else if (optionName == "-test")
                    Test = stack.Pop();
                else if (optionName == "-minmark")
                    MinMark = int.Parse(stack.Pop());
                else if (optionName == "-maxmark")
                    MaxMark = int.Parse(stack.Pop());
                else if (optionName == "-datefrom")
                    DateFrom = DateTime.ParseExact(stack.Pop(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                else if (optionName == "-dateto")
                    DateTo = DateTime.ParseExact(stack.Pop(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                else if (optionName == "-sort")
                    ParseSortingOptions(stack.Pop(), stack.Pop());
            }
        }

        private void ParseSortingOptions(string propertyName, string order)
        {
            // Capitalizing first letter of a property name
            propertyName = char.ToUpper(propertyName[0]) + propertyName.Substring(1);
            var propertyToOrderBy = typeof(StudentTest).GetProperty(propertyName);
            if (propertyToOrderBy == null)
                throw new ArgumentException($"Order by {propertyName} is not supported");

            var inAscendingOrder = Regex.IsMatch(order, "(asc|desc)")
                ? order == "asc"
                : throw new ArgumentException("Invalid argument format for -sort");
            SortingOptions = new SortingOptions(propertyToOrderBy, inAscendingOrder);
        }
    }
}
