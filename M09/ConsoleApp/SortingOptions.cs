﻿using System.Reflection;

namespace ConsoleApp
{
    internal class SortingOptions
    {
        public PropertyInfo PropertyToOrderBy { get; }
        public bool InAscendingOrder { get; }

        public SortingOptions(PropertyInfo propertyToOrderBy, bool inAscendingOrder)
        {
            PropertyToOrderBy = propertyToOrderBy;
            InAscendingOrder = inAscendingOrder;
        }
    }
}