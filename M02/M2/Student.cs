﻿using System;

namespace Students
{
    internal class Student
    {
        public string Name { get; }
        public string Email { get; }

        public Student(string name, string email)
        {
            if (NameExtractor.ExtractNameFromEmail(email) != name) throw new ArgumentException("Name and Email don't match");
            Email = email;
            Name = name;
        }

        public Student(string email) : this(NameExtractor.ExtractNameFromEmail(email), email) { }

        public override bool Equals(object obj)
        {
            if (!(obj is Student)) return false;
            var s = (Student) obj;
            return Name == s.Name && Email == s.Email;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Email != null ? Email.GetHashCode() : 0);
            }
        }
    }
}
