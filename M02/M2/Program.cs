﻿using System;
using System.Collections.Generic;

namespace Students
{
    internal class Program
    {
        private static readonly Random Random = new Random();

        private static void Main()
        {
            var student1C1 = new Student("Vasya Pupkin", "vasya.pupkin@epam.com");
            var student2C1 = new Student("Andrey Petrov", "andrey.petrov@epam.com");
            var student3C1 = new Student("Maxim Ivanov", "maxim.ivanov@epam.com");

            var student1C2 = new Student("vasya.pupkin@epam.com");
            var student2C2 = new Student("andrey.petrov@epam.com");
            var student3C2 = new Student("maxim.ivanov@epam.com");

            var studentSubjectDict = new Dictionary<Student, HashSet<string>>
            {
                [student1C1] = Get3RandomSubjects(),
                [student2C1] = Get3RandomSubjects(),
                [student3C1] = Get3RandomSubjects(),
                [student1C2] = Get3RandomSubjects(),
                [student2C2] = Get3RandomSubjects(),
                [student3C2] = Get3RandomSubjects()
            };

            Console.WriteLine(studentSubjectDict.Count);
        }

        private static HashSet<string> Get3RandomSubjects()
        {
            var subjects = new List<string>() {"Math", "Informatics", "Physics", "Biology"};
            subjects.RemoveAt(Random.Next(0, subjects.Count-1));
            var result = new HashSet<string>(subjects);
            return result;
        }
    }
}