﻿using System.Linq;

namespace Students
{
    internal class NameExtractor
    {
        public static string ExtractNameFromEmail(string email)
        {
            return string.Join(' ',
                email.Substring(0, email.IndexOf('@')).Split('.')
                    .Select(x => x.First().ToString().ToUpper() + x.Substring(1)).ToArray());
        }
    }
}