﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Performance
{
    internal class Program
    {
        private static readonly Random Random = new Random();

        private static void Main()
        {
            var classes = new C[100000];
            var structs = new S[100000];
            var integers = new int[100000];

            var start = Process.GetCurrentProcess().PrivateMemorySize64;
            for (var i = 0; i < classes.Length; i++)
            {
                classes[i] = new C(Random.Next());
            }
            var finish = Process.GetCurrentProcess().PrivateMemorySize64;
            var deltaForClasses = finish - start;
            Console.WriteLine($"PrivateMemorySize64 delta for classes = {deltaForClasses} bytes.");

            start = Process.GetCurrentProcess().PrivateMemorySize64;
            for (var i = 0; i < structs.Length; i++)
            {
                structs[i] = new S(Random.Next());
            }
            finish = Process.GetCurrentProcess().PrivateMemorySize64;
            var deltaForStructs = finish - start;
            Console.WriteLine($"PrivateMemorySize64 delta for structs = {deltaForStructs} bytes.");
            Console.WriteLine($"The difference between these deltas is {deltaForClasses-deltaForStructs} bytes.");

            for (var i = 0; i < integers.Length; i++)
            {
                integers[i] = Random.Next();
            }

            var stopwatch = Stopwatch.StartNew();
            Array.Sort(classes, (x, y) => x.i.CompareTo(y.i));
            Console.WriteLine($"Sorting(classes, Comparison) took {stopwatch.Elapsed.TotalMilliseconds.ToString(CultureInfo.InvariantCulture)} milliseconds");

            stopwatch = Stopwatch.StartNew();
            Array.Sort(structs, (x, y) => x.i.CompareTo(y.i));
            Console.WriteLine($"Sorting(structs, Comparison) took {stopwatch.Elapsed.TotalMilliseconds.ToString(CultureInfo.InvariantCulture)} milliseconds");

            stopwatch = Stopwatch.StartNew();
            Array.Sort(integers);
            Console.WriteLine($"Sorting of integers took {stopwatch.Elapsed.TotalMilliseconds.ToString(CultureInfo.InvariantCulture)} milliseconds");
        }
    }
}
