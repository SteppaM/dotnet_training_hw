﻿namespace Shapes
{
    public class Square : Rectangle
    {
        public Square(Point position, double side) : base(position, side, side)
        {
        }

        public override string ToString()
        {
            return $"{base.ToString()}";
        }
    }
}