﻿using System;

namespace Shapes
{
    public abstract class Shape
    {
        protected Shape(Point position)
        {
            Position = position;
        }

        protected Point Position { get; }
        public abstract double Perimeter { get; }
        public abstract double Area { get; }

        public override string ToString()
        {
            return $"Figure type: {GetType().Name}\n" + 
                   $"Position: {Position.ToString()}\n" +
                   $"Perimeter: {Math.Round(Perimeter, 4)}\n" +
                   $"Area: {Math.Round(Area, 4)}";
        }
    }
}
