﻿namespace Shapes
{
    public class Rectangle : Shape
    {
        public Rectangle(Point position, double side1, double side2) : base(position)
        {
            Side1 = side1;
            Side2 = side2;
        }

        public double Side1 { get; }
        public double Side2 { get; }
        public override double Perimeter => 2 * (Side1 + Side2);
        public override double Area => Side1 * Side2;

        public override string ToString()
        {
            return $"{base.ToString()}\n" +
                   $"Side1: {Side1}\n" +
                   $"Side2: {Side2}";
        }
    }
}