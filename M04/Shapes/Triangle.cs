﻿using System;

namespace Shapes
{
    public class Triangle : Shape
    {
        public Triangle(Point position, double side1, double side2, double side3) : base(position)
        {
            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
        }

        public double Side1 { get; }
        public double Side2 { get; }
        public double Side3 { get; }

        public override double Perimeter => Side1 + Side2 + Side3;
        public override double Area
        {
            get
            {
                var p = Perimeter;
                return Math.Sqrt(p * (p - Side1) * (p - Side2) * (p - Side3));
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()}\n" +
                   $"Side1: {Side1}\n" +
                   $"Side2: {Side2}\n" +
                   $"Side3: {Side3}";
        }
    }
}