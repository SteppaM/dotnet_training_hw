﻿using System;

namespace Shapes
{
    public class Circle : Shape
    {
        public Circle(Point position, double radius) : base(position)
        {
            Radius = radius;
        }

        public double Radius { get; }
        public override double Perimeter => 2 * Math.PI * Radius;
        public override double Area => Math.PI * Radius * Radius;

        public override string ToString()
        {
            return $"{base.ToString()}\n" +
                   $"Radius: {Radius}";
        }
    }
}
