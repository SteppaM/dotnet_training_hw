﻿using System.Drawing;

namespace Game.Bonuses
{
    internal class Cherry : Bonus
    {
        public Cherry(Point position, int scorePoints) : base(position, scorePoints)
        {
        }
    }
}
