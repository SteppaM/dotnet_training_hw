﻿using System.Drawing;

namespace Game.Bonuses
{
    internal class Apple : Bonus
    {
        public Apple(Point position, int scorePoints) : base(position, scorePoints)
        {
        }
    }
}
