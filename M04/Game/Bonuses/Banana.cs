﻿using System.Drawing;

namespace Game.Bonuses
{
    internal class Banana : Bonus
    {
        public Banana(Point position, int scorePoints) : base(position, scorePoints)
        {
        }
    }
}
