﻿using System.Drawing;

namespace Game.Obstacles
{
    internal class Stone : Obstacle
    {
        public Stone(Point position) : base(position)
        {
        }
    }
}
