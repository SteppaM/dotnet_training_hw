﻿using System.Drawing;

namespace Game.Obstacles
{
    internal class Tree : Obstacle
    {
        public Tree(Point position) : base(position)
        {
        }
    }
}
