﻿using System.Drawing;

namespace Game
{
    internal abstract class Bonus : GameObject
    {
        protected Bonus(Point position, int scorePoints) : base(position)
        {
            ScorePoints = scorePoints;
        }

        public int ScorePoints { get; }
    }
}