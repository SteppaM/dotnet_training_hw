﻿using System.Drawing;

namespace Game
{
    internal abstract class GameObject
    {
        protected GameObject(Point position)
        {
            Position = position;
        }

        public Point Position { get; }
    }
}
