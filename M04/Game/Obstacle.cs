﻿using System.Drawing;

namespace Game
{
    internal abstract class Obstacle : GameObject
    {
        protected Obstacle(Point position) : base(position)
        {
        }
    }
}