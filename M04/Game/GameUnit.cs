﻿using System.Drawing;

namespace Game
{
    internal abstract class GameUnit : GameObject
    {
        protected GameUnit(Point position, int health, int power) : base(position)
        {
            Health = health;
            Power = power;
        }

        public int Health { get; }
        public int Power { get; }
    }
}