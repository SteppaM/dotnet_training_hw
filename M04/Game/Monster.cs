﻿using System.Drawing;

namespace Game
{
    internal abstract class Monster : GameUnit
    {
        protected Monster(Point position, int health, int power) : base(position, health, power)
        {
        }

        // Unique hunting algorithm for each monster.
        public abstract void Hunt();
    }
}