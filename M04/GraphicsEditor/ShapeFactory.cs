﻿using Shapes;

namespace Editor
{
    public class ShapeFactory
    {
        public Circle CreateCircle(Point position, double radius)
        {
            return new Circle(position, radius);
        }

        public Triangle CreateTriangle(Point position, double side1, double side2, double side3)
        {
            return new Triangle(position, side1, side2, side3);
        }

        public Rectangle CreateRectangle(Point position, double side1, double side2)
        {
            return new Rectangle(position, side1, side2);
        }

        public Square CreateSquare(Point position, double side)
        {
            return new Square(position, side);
        }
    }
}
