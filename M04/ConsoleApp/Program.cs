﻿using System;
using Shapes;
using Editor;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {
            var shapeFactory = new ShapeFactory();

            var shapes = new Shape[]
            {
                shapeFactory.CreateCircle(new Point(1, 1), 1),
                shapeFactory.CreateRectangle(new Point(3, 3), 1, 1),
                shapeFactory.CreateSquare(new Point(5, 5), 1),
                shapeFactory.CreateTriangle(new Point(7, 7), 1, 1, 1)
            };

            foreach (var shape in shapes)
            {
                Console.WriteLine(shape);
                Console.WriteLine();
            }
        }
    }
}
