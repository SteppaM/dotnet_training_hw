using System;
using System.Linq;
using ConsoleApp;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    public class MatrixExtensionsTests
    {
        [Test]
        public void BubbleSort_NullFunc_ThrowsException()
        {
            var actualMatrix = new int[0, 0];
            Assert.That(() => actualMatrix.BubbleSort(null), Throws.Exception.TypeOf<ArgumentNullException>()); 
        }

        [Test]
        public void BubbleSort_ByMinElemInRow_ReturnsExpectedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {0, 2, 3}, // min = 0
                {1, 2, 9}, // min = 1
                {5, 5, 5}  // min = 5
            };

            actualMatrix.BubbleSort(row => row.Min());

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }

        [Test]
        public void BubbleSort_ByMinElemInRowDesc_ReturnsCorrectlySortedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5}, 
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {5, 5, 5}, // min == 5
                {1, 2, 9}, // min == 1
                {0, 2, 3}  // min == 0
            };

            actualMatrix.BubbleSort(row => row.Min(), true);

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }

        [Test]
        public void BubbleSort_ByMaxElemInRow_ReturnsExpectedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {0, 2, 3}, // max == 3
                {5, 5, 5}, // max == 5
                {1, 2, 9}  // max == 9
            };

            actualMatrix.BubbleSort(row => row.Max());

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }

        [Test]
        public void BubbleSort_ByMaxElemInRowDesc_ReturnsCorrectlySortedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {1, 2, 9},  // max == 9
                {5, 5, 5},  // max == 5
                {0, 2, 3}   // max == 3
            };

            actualMatrix.BubbleSort(row => row.Max(), true);

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }

        [Test]
        public void BubbleSort_BySumOfElemsInRow_ReturnsExpectedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {0, 2, 3}, // sum == 5
                {1, 2, 9}, // sum == 12
                {5, 5, 5}  // sum == 15
            };

            

            actualMatrix.BubbleSort(row => row.Sum());

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }

        [Test]
        public void BubbleSort_BySumOfElemsInRowDesc_ReturnsCorrectlySortedMatrix()
        {
            var actualMatrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            var expectedMatrix = new[,]
            {
                {5, 5, 5},  // sum == 15
                {1, 2, 9},  // sum == 12
                {0, 2, 3}   // sum == 5
            };

            actualMatrix.BubbleSort(row => row.Sum(), true);

            Assert.That(actualMatrix, Is.EqualTo(expectedMatrix));
        }
    }
}