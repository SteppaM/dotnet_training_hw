﻿using System;
using System.Text;

namespace ConsoleApp
{
    internal static class MatrixExtensions
    {
        /// <summary>
        /// Bubble-sorts a matrix, arranging matrix rows by implementing functions (min, max, sum, etc.) for each row and comparing results.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix">Matrix which will be sorted.</param>
        /// <param name="fun">Function which will be implemented for each row.</param>
        /// <param name="descending"></param>
        public static void BubbleSort<T>(this T[,] matrix, Func<T[], T> fun, bool descending = false)
            where T : IComparable<T>
        {
            if (fun == null) throw new ArgumentNullException(nameof(fun));

            var funToRowsResults = matrix.GetFuncToRowsResults(fun);

            for (var bypass = 1; bypass < matrix.GetLength(0); bypass++)
            {
                for (var i = 0; i < matrix.GetLength(0) - bypass; i++)
                {
                    if (funToRowsResults[i].CompareTo(funToRowsResults[i+1]) > 0 ? !descending : descending)
                    {
                        matrix.SwapRows(i, i + 1);
                        // Also swapping func results
                        (funToRowsResults[i], funToRowsResults[i + 1]) = (funToRowsResults[i+1], funToRowsResults[i]);
                    }
                }
            }
        }

        private static T[] GetFuncToRowsResults<T>(this T[,] matrix, Func<T[], T> fun)
        {
            var funToRowsResults = new T[matrix.GetLength(0)];
            var argumentArray = new T[matrix.GetLength(1)];
            for (var rowIdx = 0; rowIdx < funToRowsResults.Length; rowIdx++)
            {
                for (var colIdx = 0; colIdx < matrix.GetLength(0); colIdx++)
                {
                    argumentArray[colIdx] = matrix[rowIdx, colIdx];
                }

                funToRowsResults[rowIdx] = fun(argumentArray);
            }

            return funToRowsResults;
        }

        private static void SwapRows<T>(this T[,] matrix, int idx1, int idx2)
        {
            for (var columnIdx = 0; columnIdx < matrix.GetLength(1); columnIdx++)
            {
                (matrix[idx1, columnIdx], matrix[idx2, columnIdx]) = (matrix[idx2, columnIdx], matrix[idx1, columnIdx]);
            }
        }

        public static string ToMyString<T>(this T[,] matrix)
        {
            if (matrix == null || matrix.Length == 0)
                return "";

            var sb = new StringBuilder();
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    sb.Append($"{matrix[i, j]} ");
                }

                sb.Append('\n');
            }

            return sb.ToString();
        }
    }
}