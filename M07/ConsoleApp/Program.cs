﻿using System;
using System.Linq;
using ConsoleApp.Countdown;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {
            // Initial matrix
            var matrix = new[,]
            {
                {5, 5, 5},
                {0, 2, 3},
                {1, 2, 9}
            };

            Console.WriteLine("Initial matrix:\n" + matrix.ToMyString());
            matrix.BubbleSort(row => row.Sum());
            Console.WriteLine("Sorted by sum of elements in a row:\n" + (matrix.ToMyString()));
            matrix.BubbleSort(row => row.Sum(), true);
            Console.WriteLine("Sorted by sum of elements in a row (descending):\n" + (matrix.ToMyString()));
            matrix.BubbleSort(row => row.Min());
            Console.WriteLine("Sorted by min element in a row:\n" + (matrix.ToMyString()));
            matrix.BubbleSort(row => row.Min(), true);
            Console.WriteLine("Sorted by min element in a row (descending):\n" + (matrix.ToMyString()));
            matrix.BubbleSort(row => row.Max());
            Console.WriteLine("Sorted by max element in a row:\n" + (matrix.ToMyString()));
            matrix.BubbleSort(row => row.Max(), true);
            Console.WriteLine("Sorted by max element in a row (descending):\n" + (matrix.ToMyString()));

            // Countdown class example
            var countdown = new Countdown.Countdown(5);
            var subscriber1 = new Subscriber("Sub1");
            var subscriber2 = new Subscriber("Sub2");

            countdown.Message += subscriber1.Update;
            countdown.Message += subscriber2.Update;

            countdown.NotifyObservers("Hello");
        }
    }
}
