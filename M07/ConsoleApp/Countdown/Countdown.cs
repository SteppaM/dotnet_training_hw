﻿using System;
using System.Threading;
using ConsoleApp.Countdown.Interfaces;

namespace ConsoleApp.Countdown
{
    internal class Countdown : IMyObservable<string>
    {
        public Countdown(int delayTimeInSeconds)
        {
            if (DelayTimeInSeconds < 0 || DelayTimeInSeconds > 1000)
                throw new ArgumentOutOfRangeException(nameof(DelayTimeInSeconds),
                    "Argument should be in range from 0 to 1000");

            DelayTimeInSeconds = delayTimeInSeconds;
        }

        public event EventHandler<string> Message;
        public int DelayTimeInSeconds { get; }

        public void RegisterObserver(IMyObserver<string> observer)
        {
            Message += observer.Update;
        }

        public void RemoveObserver(IMyObserver<string> observer)
        {
            Message -= observer.Update;
        }

        public void NotifyObservers(string message)
        {
            for (var i = DelayTimeInSeconds; i >= 0; i--)
            {
                Console.Write($"Sending message to subscribers in {i} seconds.    \r");
                Thread.Sleep(1000);
            }

            Message?.Invoke(this, message);
        }
    }
}
