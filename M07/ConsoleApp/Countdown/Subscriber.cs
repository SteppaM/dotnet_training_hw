﻿using System;
using ConsoleApp.Countdown.Interfaces;

namespace ConsoleApp.Countdown
{
    internal class Subscriber : IMyObserver<string>
    {
        public Subscriber(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public void Update(object sender, string message)
        {
            Console.WriteLine($"Countdown successfully transmitted message to subscriber with name: {Name}");
            Console.WriteLine($"Message text: {message}");
        }
    }
}