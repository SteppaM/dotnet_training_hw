﻿namespace ConsoleApp.Countdown.Interfaces
{
    internal interface IMyObservable<T>
    {
        void RegisterObserver(IMyObserver<T> observer);
        void RemoveObserver(IMyObserver<T> observer);
        void NotifyObservers(T message);
    }
}
