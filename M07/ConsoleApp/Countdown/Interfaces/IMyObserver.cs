﻿namespace ConsoleApp.Countdown.Interfaces
{
    internal interface IMyObserver<T>
    {
        void Update(object sender, T args);
    }
}