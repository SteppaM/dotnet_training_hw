using System;
using NUnit.Framework;
using Parsers;

namespace TestProject
{
    [TestFixture]
    internal class IntParserTests
    {
        [Test]
        public void Parse_Null_ThrowsException()
        {
            Assert.That(() => IntParser.Parse(null), Throws.ArgumentNullException);
        }

        [Test]
        public void Parse_EmptyString_ThrowsException()
        {
            Assert.That(() => IntParser.Parse(""), Throws.ArgumentException);
        }

        [TestCase("!123@")]
        public void Parse_InvalidFormatArg_ThrowsException(string number)
        {
            Assert.That(() => IntParser.Parse(number), Throws.Exception.TypeOf<FormatException>());
        }

        [TestCase("123123123112121212211")]
        public void Parse_TooBigArg_ThrowsException(string number)
        {
            Assert.That(() => IntParser.Parse(number), Throws.Exception.TypeOf<OverflowException>());
        }

        [TestCase("-123123123112121212211")]
        public void Parse_TooSmallArg_ThrowsException(string number)
        {
            Assert.That(() => IntParser.Parse(number), Throws.Exception.TypeOf<OverflowException>());
        }

        [TestCase("+123", 123)]
        [TestCase("-123", -123)]
        public void Parse_ValidArg_ReturnsExpected(string number, int expected)
        {
            Assert.That(IntParser.Parse(number), Is.EqualTo(expected));
        }
    }
}