﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NLog;
using NLog.Config;
using NLog.Targets;

[assembly: InternalsVisibleTo("ConsoleApp")]

namespace Parsers
{
    internal static class IntParser
    {
        public static int Parse(string str)
        {
            var config = new LoggingConfiguration();
            var fileTarget = new FileTarget("target1")
            {
                FileName = "${basedir}/../../../../logs/${shortdate}.log",
                Layout = "${longdate} | ${uppercase:${level}} | ${logger} | ${message}"
            };

            config.AddTarget(fileTarget);
            config.AddRuleForAllLevels(fileTarget);
            LogManager.Configuration = config;
            var logger = LogManager.GetCurrentClassLogger();

            try
            {
                logger.Debug("User made a request.");
                logger.Debug("Application Started.");

                if (str == null) throw new ArgumentNullException(nameof(str), "Input string is null.");
                if (str.Length == 0) throw new ArgumentException("Input string is empty.");

                var trimmedStr = str.Trim();
                var correctFormatRegex = new Regex(@"^[-+]?\d+$");

                if (!correctFormatRegex.IsMatch(trimmedStr))
                    throw new FormatException("Input string was not in a correct format.");

                var digits = Regex.Match(trimmedStr, @"\d+").Value;

                var sign = 1;
                if (trimmedStr[0] == '-')
                    sign = -1;

                var absoluteValue = 0;
                foreach (var digit in digits)
                    checked { absoluteValue = absoluteValue * 10 + digit - '0'; }

                logger.Debug("Responce was returned");

                return sign * absoluteValue;
            }
            catch (ArgumentNullException e)
            {
                logger.Error(e.Message);
                throw;
            }
            catch (ArgumentException e)
            {
                logger.Error(e.Message);
                throw;
            }
            catch (FormatException e)
            {
                logger.Error(e.Message);
                throw;
            }
            catch (OverflowException e)
            {
                logger.Error(e.Message);
                throw;
            }
        }
    }
}
