﻿using System;
using Parsers;

namespace ConsoleApp
{
    internal class Program
    {
        static void Main()
        {
            var args = new[]
            {
                null,
                "",
                "!123@",
                "123123123112121212211",
                "-12312312311212121221",
                "+123123123",
                "-123123123"
            };

            foreach (var arg in args)
            {
                try
                {
                    Console.WriteLine($"Argument string = {(arg == null ? "null" : $"\"{arg}\"")}");
                    Console.WriteLine($"Output: {IntParser.Parse(arg)}");
                    Console.WriteLine();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception. Message: {e.Message}");
                    Console.WriteLine();
                }
            }
        }
    }
}
