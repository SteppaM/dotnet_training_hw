﻿using System.Collections.Generic;
using System.Linq;
using MyGenerics;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    internal class FibonacciGeneratorTests
    {
        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(47)]
        public void Generate_OutOfRangeOrZeroLength_ReturnsEmptySequence(int length)
        {
            var fibNumbers = FibonacciGenerator.Generate(length);
            Assert.That(fibNumbers.ToList().Count, Is.Zero);
        }

        [Test]
        public void Generate_ValidArg_ReturnsIEnumerable()
        {
            var expectedFibNumbers = new List<int>() {1, 1, 2, 3, 5};
            var actualFibNumbers = FibonacciGenerator.Generate(5).ToList();
            Assert.That(actualFibNumbers, Is.EqualTo(expectedFibNumbers));
        }
    }
}
