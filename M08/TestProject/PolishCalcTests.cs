﻿using MyGenerics;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture]
    internal class PolishCalcTests
    {
        [TestCase("5 1 2 + 4 * + 3 -", 14)]
        [TestCase("3 -", -3)]
        [TestCase("3 +", 3)]
        [TestCase("  ", 0)]
        public void CalcTest(string expr, double expectedResult)
        {
            var actualResult = PolishCalc.Evaluate(expr);
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }
    }
}
