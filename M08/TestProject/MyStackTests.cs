﻿using MyGenerics;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture()]
    internal class MyStackTests
    {
        [Test]
        public void PushPopPeekTest()
        {
            var myStack = new MyStack<int>();
            myStack.Push(1);
            Assert.Multiple((() =>
            {
                Assert.That(myStack.Count, Is.EqualTo(1));
                Assert.That(myStack.Peek(), Is.EqualTo(1));
                Assert.That(myStack.Count, Is.EqualTo(1));
                Assert.That(myStack.Pop(), Is.EqualTo(1));
                Assert.That(myStack.Count, Is.EqualTo(0));
            }));
        }
    }
}
