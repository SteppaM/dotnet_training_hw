using System.Collections.Generic;
using NUnit.Framework;
using MyGenerics;

namespace TestProject
{
    [TestFixture]
    internal class MyBinarySearchTests
    {
        [TestCase(1, 0)]
        [TestCase(9, 8)]
        [TestCase(5, 4)]
        [TestCase(0, -1)]
        [TestCase(10, -1)]
        public void MyBinarySearch_BuiltInTypeDefaultCompare_ReturnsIdx(int value, int expectedIdx)
        {
            var intList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var actualIdx = intList.MyBinarySearch(value);

            Assert.That(actualIdx, Is.EqualTo(expectedIdx));
        }

        [TestCase(1, 0)]
        [TestCase(9, 8)]
        [TestCase(5, 4)]
        [TestCase(0, -1)]
        [TestCase(10, -1)]
        public void MyBinarySearch_BuiltInTypeCustomCompare_ReturnsIdx(int value, int expectedIdx)
        {
            var intList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var actualIdx = intList.MyBinarySearch(value, (x, y) => x-y);

            Assert.That(actualIdx, Is.EqualTo(expectedIdx));
        }

        [Test]
        public void MyBinarySearch_CustomTypeDefaultCompare_ThrowsException()
        {
            var myList = new List<MyClass>
            {
                new MyClass(1),
                new MyClass(2),
                new MyClass(3),
                new MyClass(4),
                new MyClass(5),
                new MyClass(6),
                new MyClass(7)
            };

            var value = new MyClass(1);

            Assert.That(() => myList.MyBinarySearch(value), Throws.InvalidOperationException);
        }

        [Test]
        public void MyBinarySearch_CustomTypeCustomCompare_ReturnsIdx()
        {
            var myList = new List<MyClass>
            {
                new MyClass(1),
                new MyClass(2),
                new MyClass(3),
                new MyClass(4),
                new MyClass(5),
                new MyClass(6),
                new MyClass(7)
            };
            
            var value = new MyClass(1);
            var expectedIdx = 0;

            var actualIdx = myList.MyBinarySearch(value, (x, y) => x.Value - y.Value);
            Assert.That(actualIdx, Is.EqualTo(expectedIdx));
        }

        public class MyClass
        {
            public int Value;

            public MyClass(int value)
            {
                this.Value = value;
            }
        }
    }
}