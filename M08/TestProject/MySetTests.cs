﻿using MyGenerics;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture()]
    internal class MySetTests
    {
        [Test]
        public void AddRemoveContainsTest()
        {
            var mySet = new MySet<string>();
            Assert.Multiple((() =>
            {
                Assert.That(mySet.Contains("Stepa"), Is.False);
                mySet.Add("Stepa");
                mySet.Add("Stepa");
                Assert.That(mySet.Count, Is.EqualTo(1));
                Assert.That(mySet.Contains("Stepa"), Is.True);
                mySet.Remove("Stepa");
                Assert.That(mySet.Contains("Stepa"), Is.False);
            }));
        }

        [Test]
        public void RehashTest()
        {
            var mySet = new MySet<string>(1);
            for (var i = 0; i < 100; i++)
            {
                mySet.Add(i.ToString());
            }
            Assert.Multiple((() =>
            {
                Assert.That(mySet.Count, Is.EqualTo(100));
                Assert.That(mySet.Count, Is.LessThan(mySet.Threshold));
            }));
        }

        [Test]
        public void UnionWithTest()
        {
            var mySet = new MySet<string>();
            var argumentSet = new MySet<string>();

            mySet.Add("1");
            argumentSet.Add("1");
            argumentSet.Add("2");

            mySet.UnionWith(argumentSet);
            Assert.Multiple((() =>
            {
                Assert.That(mySet.Count, Is.EqualTo(2));
                Assert.That(mySet.Contains("1"));
                Assert.That(mySet.Contains("2"));
            }));
        }

        [Test]
        public void IntersectWithTest()
        {
            var mySet = new MySet<string>();
            var argumentSet = new MySet<string>();

            mySet.Add("1");
            mySet.Add("2");
            argumentSet.Add("2");
            argumentSet.Add("3");

            mySet.IntersectWith(argumentSet);

            Assert.Multiple((() =>
            {
                Assert.That(mySet.Count, Is.EqualTo(1));
                Assert.That(mySet.Contains("2"));
            }));
        }

        [Test]
        public void ExceptWithTest()
        {
            var mySet = new MySet<string>();
            var argumentSet = new MySet<string>();

            mySet.Add("1");
            mySet.Add("2");
            argumentSet.Add("2");
            argumentSet.Add("3");

            mySet.ExceptWith(argumentSet);

            Assert.Multiple((() =>
            {
                Assert.That(mySet.Count, Is.EqualTo(1));
                Assert.That(mySet.Contains("1"));
            }));
        }

        [Test]
        public void SymmetricExceptWithTest()
        {
            var mySet = new MySet<string>();
            var argumentSet = new MySet<string>();

            mySet.Add("1");
            mySet.Add("2");
            argumentSet.Add("2");
            argumentSet.Add("3");

            mySet.SymmetricExceptWith(argumentSet);

            Assert.Multiple((() =>
            {
                Assert.That(mySet.Count, Is.EqualTo(2));
                Assert.That(mySet.Contains("1"));
                Assert.That(mySet.Contains("3"));
            }));
        }
    }
}
