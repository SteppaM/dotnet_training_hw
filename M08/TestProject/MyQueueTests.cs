﻿using System.Linq;
using MyGenerics;
using NUnit.Framework;

namespace TestProject
{
    [TestFixture()]
    internal class MyQueueTests
    {
        [Test]
        public void EnqueuePeekTest()
        {
            var myQueue = new MyQueue<int>();
            myQueue.Enqueue(1);
            myQueue.Enqueue(2);
            Assert.Multiple((() =>
            {
                Assert.That(myQueue.Count, Is.EqualTo(2));
                Assert.That(myQueue.Peek(), Is.EqualTo(1));
                //
                Assert.That(myQueue.Count, Is.EqualTo(2));
                Assert.That(myQueue.Peek(), Is.EqualTo(1));
            }));
        }

        [Test]
        public void DequeueTest()
        {
            var myQueue = new MyQueue<int>(Enumerable.Range(1, 3));
            Assert.Multiple((() =>
            {
                Assert.That(myQueue.Dequeue(), Is.EqualTo(1));
                Assert.That(myQueue.Dequeue(), Is.EqualTo(2));
                Assert.That(myQueue.Dequeue(), Is.EqualTo(3));
            }));
        }
    }
}
