﻿using System;
using System.Collections.Generic;

namespace MyGenerics
{
    public static class MyListExtensions
    {
        public static int MyBinarySearch<T>(this List<T> list, T value, Func<T, T, int> compare = null)
        {
            if (compare == null)
                compare = Comparer<T>.Default.Compare;

            var left = 0;
            var right = list.Count - 1;
            while (left <= right)
            {
                var mid = left + ((right - left) / 2);

                try
                {
                    if (compare(value, list[mid]) == 0)
                        return mid;
                    if (compare(value, list[mid]) < 0)
                        right = mid - 1;
                    else
                        left = mid + 1;
                }
                catch (ArgumentException e)
                {
                    throw new InvalidOperationException("Failed to compare two elements in array.", e);
                }
            }

            return -1;
        }
    }
}
