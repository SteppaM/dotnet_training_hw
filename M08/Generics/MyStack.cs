﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyGenerics
{
    public class MyStack<T> : IEnumerable<T>
    {
        private List<T> Items { get; set; } = new List<T>();
        public int Count => Items.Count;

        public MyStack() { }

        public MyStack(IEnumerable<T> items)
        {
            foreach (var item in items)
                Push(item);
        }

        public T Peek()
        {
            if (Count == 0) throw new InvalidOperationException("Stack is empty.");
            return Items[Count - 1];
        }

        public T Pop()
        {
            if (Count == 0) throw new InvalidOperationException("Stack is empty.");
            var result = Items[Count - 1];
            Items.RemoveAt(Count-1);
            return result;
        }

        public void Push(T item) => Items.Add(item);

        public void Clear() => Items = new List<T>();

        public IEnumerator<T> GetEnumerator() => new MyEnumerator(this);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private class MyEnumerator : IEnumerator<T>
        {
            private int _index = -1;
            private List<T> _items;

            public MyEnumerator(MyStack<T> stack)
            {
                _items = stack.Items;
            }

            public bool MoveNext() => ++_index < _items.Count;

            public T Current
            {
                get
                {
                    if (_index < 0 || _index >= _items.Count)
                        return default(T);
                    return _items[_index];
                }
            }

            object IEnumerator.Current => Current;

            public void Reset()
            {
                _index = -1;
            }

            public void Dispose() {}
        }
    }
}
