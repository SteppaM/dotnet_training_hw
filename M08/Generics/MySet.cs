﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyGenerics
{
    public class MySet<T> : IEnumerable<T> where T : class
    {
        private Node<T>[] Buckets { get; set; }
        private const int InitialCapacity = 16;
        private const double DefaultLoadFactor = 0.75;
        private double LoadFactor;
        public int Count { get; private set; }
        public int CurrentCapacity => Buckets.Length;
        public double Threshold => CurrentCapacity * LoadFactor;


        public MySet(int capacity = InitialCapacity, double loadFactor = DefaultLoadFactor)
        {
            if (capacity <= 0 ) throw new ArgumentException("Capacity should be a positive integer number.");
            LoadFactor = loadFactor;
            Buckets = new Node<T>[capacity];
        }

        public MySet(IEnumerable<T> items) : this()
        {
            foreach (var item in items)
                Add(item);
        }

        public bool Contains(T item)
        {
            var index = Math.Abs(item.GetHashCode()) % CurrentCapacity;
            var bucket = Buckets[index];

            while (bucket != null)
            {
                if (bucket.Value.Equals(item))
                    return true;
                bucket = bucket.Next;
            }

            return false;
        }

        public void Add(T item)
        {
            var index = Math.Abs(item.GetHashCode()) % CurrentCapacity;
            var bucket = Buckets[index];
            var newNode = new Node<T>(item);

            if (bucket == null)
            {
                Buckets[index] = newNode;
                Count++;
                return;
            }

            while (bucket.Next != null)
            {
                if (bucket.Value.Equals(item))
                {
                    return;
                }
                bucket = bucket.Next;
            }

            if (!bucket.Value.Equals(item))
            {
                bucket.Next = newNode;
                Count++;
            }

            if (Count >= Threshold)
                Rehash();
        }

        public T Remove(T item)
        {
            var index = Math.Abs(item.GetHashCode()) % CurrentCapacity;
            var bucket = Buckets[index];

            if (bucket == null)
            {
                throw new ArgumentException("No Element Found");
            }

            if (bucket.Value.Equals(item))
            {
                Buckets[index] = bucket.Next;
                Count--;
                return item;
            }

            var prev = bucket;

            while (bucket != null)
            {
                if (bucket.Value.Equals(item))
                {
                    prev.Next = bucket.Next;
                    Count--;
                    return item;
                }
                prev = bucket;
                bucket = bucket.Next;
            }
            return default;
        }

        public void UnionWith(IEnumerable<T> other)
        {
            foreach (var item in other)
                Add(item);
        }

        public void IntersectWith(IEnumerable<T> other)
        {
            var otherSet = new MySet<T>(other);
            foreach (var item in this)
                if (!otherSet.Contains(item))
                    Remove(item);
        }

        public void ExceptWith(IEnumerable<T> other)
        {
            var otherSet = new MySet<T>(other);
            foreach (var item in this)
                if (otherSet.Contains(item))
                    Remove(item);
        }

        public void SymmetricExceptWith(IEnumerable<T> other)
        {
            var intersection = new MySet<T>(other);
            intersection.IntersectWith(this);
            UnionWith(other);
            ExceptWith(intersection);
        }

        public void Clear()
        {
            Buckets = new Node<T>[InitialCapacity];
            Count = 0;
        }

        private void Rehash()
        {
            var oldBuckets = Buckets;
            Buckets = new Node<T>[CurrentCapacity * 2];
            Count = 0;

            foreach (var bucket in oldBuckets)
            {
                var tmpBucket = bucket;
                while (tmpBucket != null)
                {
                    Add(tmpBucket.Value);
                    tmpBucket = tmpBucket.Next;
                }
            }
        }

        private class Node<T>
        {
            public Node(T value, Node<T> next = null)
            {
                Value = value;
                Next = next;
            }

            public T Value { get; }
            public Node<T> Next { get; set; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var bucket in Buckets)
            {
                var tmpBucket = bucket;
                while (tmpBucket != null)
                {
                    yield return tmpBucket.Value;
                    tmpBucket = tmpBucket.Next;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
