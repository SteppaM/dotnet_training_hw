﻿using System.Collections.Generic;

namespace MyGenerics
{
    public static class FibonacciGenerator
    {
        public static IEnumerable<int> Generate(int length)
        {

            if (length <= 0 || length > 46) yield break;

            var first = 0;
            var second = 1;

            for (var i = 0; i < length; i++)
            {
                yield return second;
                (first, second) = (second, second + first);
            }
        }
    }
}
