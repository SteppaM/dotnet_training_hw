﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyGenerics
{
    public class MyQueue<T> : IEnumerable<T>
    {
        private QueueItem<T> _head;
        private QueueItem<T> _tail;
        public int Count { get; private set; }
        public bool IsEmpty => _head == null;

        public MyQueue() { }

        public MyQueue(IEnumerable<T> items)
        {
            foreach (var item in items)
                Enqueue(item);
        }

        public void Enqueue(T value)
        {
            if (IsEmpty)
                _tail = _head = new QueueItem<T> { Value = value, Next = null };
            else
            {
                var item = new QueueItem<T> { Value = value, Next = null };
                _tail.Next = item;
                _tail = item;
            }

            Count++;
        }

        public T Dequeue()
        {
            if (_head == null) throw new InvalidOperationException();
            var result = _head.Value;
            _head = _head.Next;
            if (_head == null)
                _tail = null;
            Count--;
            return result;
        }

        public T Peek()
        {
            if (_head == null) throw new InvalidOperationException();
            return _head.Value;
        }

        public void Clear()
        {
            _head = _tail = null;
            Count = 0;
        }

        public void Add(T value) => Enqueue(value);

        private class QueueItem<T>
        {
            public T Value { get; set; }
            public QueueItem<T> Next { get; set; }
        }

        public IEnumerator<T> GetEnumerator() => new MyEnumerator(this);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private class MyEnumerator : IEnumerator<T>
        {
            private QueueItem<T> _item;
            private MyQueue<T> _queue;

            public MyEnumerator(MyQueue<T> queue)
            {
                _queue = queue;
                _item = new QueueItem<T>() { Value = default, Next = queue._head};
            }

            public bool MoveNext()
            {
                _item = _item.Next;
                return _item != null;
            }

            public T Current => _item.Value;

            object IEnumerator.Current => Current;

            public void Reset()
            {
                _item = _queue._head;
            }

            public void Dispose() { }
        }
    }
}
