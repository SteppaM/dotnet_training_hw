﻿using System.Text.RegularExpressions;

namespace MyGenerics
{
    public static class PolishCalc
    {
        public static double Evaluate(string expression)
        {
            var trimmedExpr = expression.Trim();
            if (trimmedExpr.Length == 0)
                return 0;
            var items = trimmedExpr.Split();
            var stack = new MyStack<double>();
            foreach (var item in items)
            {
                switch (item)
                {
                    case "+":
                        stack.Push(stack.Pop() + (stack.Count == 0 ? 0 : stack.Pop()));
                        break;
                    case "-":
                        stack.Push(-stack.Pop() + (stack.Count == 0 ? 0 : stack.Pop()));
                        break;
                    case "*":
                        stack.Push(stack.Pop() * stack.Pop());
                        break;
                    case "/":
                        stack.Push(1/stack.Pop() * stack.Pop());
                        break;
                    case var number when new Regex(@"\d+").IsMatch(number):
                        stack.Push(double.Parse(number));
                        break;
                }
            }

            return stack.Pop();
        }
    }
}
