﻿using System;
using System.Collections.Generic;
using MyGenerics;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main()
        {

            #region BinarySearch example

            Console.WriteLine("Binary search example");
            var intList = new List<int>(){ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Console.Write("List: ");
            foreach (var item in intList)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            var elem = 1;
            Console.WriteLine($"The index of int number {elem} is {intList.MyBinarySearch(elem)}.");
            elem = 8;
            Console.WriteLine($"The index of int number {elem} is {intList.MyBinarySearch(elem)}.\n");
            #endregion

            #region Fibonacci example

            Console.WriteLine("Fibonacci example");
            var n = 10;
            Console.WriteLine($"First {n} Fibonacci numbers:");
            foreach (var fib in FibonacciGenerator.Generate(10))
            {
                Console.Write(fib + " ");
            }
            Console.WriteLine('\n');

            #endregion

            #region Stack example

            Console.WriteLine("Stack example");
            var myStack = new MyStack<int>();
            var stackItem = 1;
            myStack.Push(stackItem);
            Console.WriteLine($"Pushed {stackItem++}. Count = {myStack.Count}.");
            myStack.Push(stackItem);
            Console.WriteLine($"Pushed {stackItem++}. Count = {myStack.Count}.");
            myStack.Push(stackItem);
            Console.WriteLine($"Pushed {stackItem}. Count = {myStack.Count}.");
            Console.Write("Stack items: ");
            foreach (var item in myStack)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
            Console.WriteLine($"Peeked {myStack.Peek()}. Count = {myStack.Count}.");
            Console.WriteLine($"Popped {myStack.Pop()}. Count = {myStack.Count}.");
            Console.WriteLine($"Popped {myStack.Pop()}. Count = {myStack.Count}.");
            Console.WriteLine($"Popped {myStack.Pop()}. Count = {myStack.Count}.");
            Console.WriteLine();

            #endregion

            #region Queue example

            Console.WriteLine("Queue example");
            var myQueue = new MyQueue<int>();
            var queueItem = 1;
            myQueue.Enqueue(queueItem);
            Console.WriteLine($"Enqueued {queueItem++}. Count = {myQueue.Count}.");
            myQueue.Enqueue(queueItem);
            Console.WriteLine($"Enqueued {queueItem++}. Count = {myQueue.Count}.");
            myQueue.Enqueue(queueItem);
            Console.WriteLine($"Enqueued {queueItem}. Count = {myQueue.Count}.");
            Console.Write("Queue items: ");
            foreach (var item in myQueue)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
            Console.WriteLine($"Dequeued {myQueue.Dequeue()}. Count = {myQueue.Count}.");
            Console.WriteLine($"Dequeued {myQueue.Dequeue()}. Count = {myQueue.Count}.");
            Console.WriteLine($"Dequeued {myQueue.Dequeue()}. Count = {myQueue.Count}.");
            Console.WriteLine();

            #endregion

            #region Set example

            Console.WriteLine("Set example");

            var set1 = new MySet<string>();
            var set2 = new MySet<string>();
            var set3 = new MySet<string>();
            var set4 = new MySet<string>();
            var argumentSet = new MySet<string>();

            for (var i = 0; i < 3; i++)
                argumentSet.Add(i.ToString());

            for (var i = 2; i < 5; i++)
            {
                set1.Add(i.ToString());
                set2.Add(i.ToString());
                set3.Add(i.ToString());
                set4.Add(i.ToString());
            }

            Console.Write($"argumentSet contains {argumentSet.Count} elements: ");
            DisplaySet(argumentSet);
            Console.Write($"set1, set2, set3 and set4 are equal sets with {set1.Count} elements: ");
            DisplaySet(set1);
            set1.UnionWith(argumentSet);
            Console.WriteLine($"set1.UnionWith(argumentSet)\nset1 contains {set1.Count} elements: ");
            DisplaySet(set1);

            set2.IntersectWith(argumentSet);
            Console.WriteLine($"set2.IntersectWith(argumentSet)\nset2 contains {set2.Count} elements: ");
            DisplaySet(set2);

            set3.ExceptWith(argumentSet);
            Console.WriteLine($"set3.ExceptWith(argumentSet)\nset3 contains {set3.Count} elements: ");
            DisplaySet(set3);

            set4.SymmetricExceptWith(argumentSet);
            Console.WriteLine($"set4.SymmetricExceptWith(argumentSet)\nset4 contains {set4.Count} elements: ");
            DisplaySet(set4);

            void DisplaySet<T>(MySet<T> set) where T : class
            {
                Console.Write("{");
                foreach (var item in set)
                    Console.Write($" {item}");
                Console.WriteLine(" }\n");
            }


            #endregion

            #region Reverse Polish notation calc example

            Console.WriteLine("Reverse Polish notation calc example");
            var expr = "5 1 2 + 4 * + 3 -";
            Console.WriteLine($"Expression = {expr}");
            var result = PolishCalc.Evaluate(expr);
            Console.WriteLine($"Result = {result}");

            #endregion
        }
    }
}
