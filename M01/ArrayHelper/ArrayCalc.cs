﻿using System;

namespace ArrayHelper
{
    public static class ArrayCalc
    {
        /// <summary>
        /// Calculates sum of positive numbers in two dimensional array.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int SumOfPosNumbers(int[,] array)
        {
            try
            {
                if (array == null) throw new ArgumentNullException(nameof(array), "Argument is null.");
                var sum = 0;
                foreach (var elem in array)
                {
                    if (elem >= 0)
                        sum += elem;
                }
                return sum;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }
    }
}