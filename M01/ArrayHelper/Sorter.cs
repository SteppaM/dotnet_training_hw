﻿using System;

namespace ArrayHelper
{
    public static class Sorter
    {
        public static void Sort(int[] array, bool descending = false)
        {
            try
            {
                if (array == null) throw new ArgumentNullException(nameof(array), "Argument is null.");
                for (var bypass = 1; bypass < array.Length; bypass++)
                {
                    for (var k = 0; k < array.Length - bypass; k++)
                    {
                        if ((!descending && array[k] > array[k + 1]) ||
                            (descending && array[k] < array[k + 1]))
                        {
                            var tempValue = array[k + 1];
                            array[k + 1] = array[k];
                            array[k] = tempValue;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}