﻿using System;
using System.Linq;
using ArrayHelper;
using RectangleHelper;

namespace M1
{
    class Program
    {
        static void Main()
        {
            int[] array = new[] { -2, 4, 5, -3, 1, 3, 2, -1, 0 };

            // Ascending sorting
            Sorter.Sort(array);
            Console.WriteLine(string.Join(' ', array));

            // Descending sorting
            Sorter.Sort(array, true);
            Console.WriteLine(string.Join(' ', array));

            // Null test
            Sorter.Sort(null);

            var twoDimensionalArr = new int[2, 3]
            {
                {1, -2, -3},
                {4, -5, 0}
            };

            Console.WriteLine(ArrayCalc.SumOfPosNumbers(twoDimensionalArr));
            // Argument == null test
            Console.WriteLine(ArrayCalc.SumOfPosNumbers(null));

            Console.WriteLine(RectangleCalc.Perimeter(4, 5));
            // Invalid args test
            Console.WriteLine(RectangleCalc.Perimeter(4, -1));

            Console.WriteLine(RectangleCalc.Square(4, 5));
            // Invalid args test
            Console.WriteLine(RectangleCalc.Square(4, -1));
        }
    }
}
