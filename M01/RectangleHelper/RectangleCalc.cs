﻿using System;

namespace RectangleHelper
{
    public class RectangleCalc
    {
        public static int Perimeter(int a, int b)
        {
            try
            {
                if (a <= 0) throw new ArgumentException("Argument should be a positive number", nameof(a));
                if (b <= 0) throw new ArgumentException("Argument should be a positive number", nameof(b));
                return (a + b) * 2;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }

        public static int Square(int a, int b)
        {
            try
            {
                if (a <= 0) throw new ArgumentException("Argument should be a positive number", nameof(a));
                if (b <= 0) throw new ArgumentException("Argument should be a positive number", nameof(b));
                return a * b;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return -1;
            }
        }
    }
}